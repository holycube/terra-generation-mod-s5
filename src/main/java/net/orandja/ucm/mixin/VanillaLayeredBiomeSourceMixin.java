package net.orandja.ucm.mixin;

import net.minecraft.util.registry.BuiltinRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.layer.BiomeLayers;
import net.minecraft.world.biome.source.BiomeLayerSampler;
import net.minecraft.world.biome.source.VanillaLayeredBiomeSource;
import net.orandja.ucm.UCM;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(VanillaLayeredBiomeSource.class)
public class VanillaLayeredBiomeSourceMixin {
    @Shadow private /*final*/ BiomeLayerSampler biomeSampler;
    @Shadow private /*final*/ Registry<Biome> biomeRegistry;
    @Shadow private /*final*/ long seed;
    @Shadow private /*final*/ boolean legacyBiomeInitLayer;
    @Shadow private /*final*/ boolean largeBiomes;

        /*public VanillaLayeredBiomeSourceMixin(long l, boolean bl, boolean bl2) {
            this.field_24728 = l;
            this.field_24498 = bl;
            this.field_24729 = bl2;
            this.biomeSampler = BiomeLayers.build(l, bl, bl2 ? 6 : 4, 4);
        }*/

    @Overwrite
    public Biome getBiomeForNoiseGen(int biomeX, int biomeY, int biomeZ) {
//        if (this.largeBiomes) {
            if (UCM.biomeSampler == null) {
                System.out.println("Created biomeSampler");
                UCM.biomeSampler = BiomeLayers.build(seed, legacyBiomeInitLayer, 5, 4);
            }

            return UCM.biomeSampler.sample(this.biomeRegistry, biomeX, biomeZ);
//        }
//
//        return this.biomeSampler.sample(this.biomeRegistry, biomeX, biomeZ);
    }
}
